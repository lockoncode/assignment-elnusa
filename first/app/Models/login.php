<?php
namespace App\Models;

use App\Helpers\AppHelper;

class Login {
    public $helper;
    public function __construct() {
        $this->helper = new AppHelper();
        $this->userMap = ['uid','name','email','password'];
    }

    public function isLogedin() { // mengecek apakah memiliki akses login atau tidak
        if(isset($_SESSION['uid'])) {
            $uid = $_SESSION['uid'];
            $col_index_uid = array_search('uid', $this->userMap); // menerjemahkan kolom ke index array
            $isFound = $this->helper->searchData($uid, $col_index_uid, 'user'); // mengecek kebenaran session
            if ($isFound) {
                return true;
            }
        } else {
            // logout ketika session tidak sesuai dengan dataset 
            session_destroy();
            return false;
        }
    }
    public function login($uid, $password) { // submit username dan password
        $helper = $this->helper;
        if(!isset($_SESSION['uid'])) {
            $col_index_uid = array_search('uid', $this->userMap);
            $col_index_password = array_search('password', $this->userMap);
            $isFound = $helper->searchData($uid, $col_index_uid, 'user');
            if ($isFound) {
                if (isset($isFound[$col_index_password]) && // mengecek kesesuain password
                    $helper->removeWhiteSpace($isFound[$col_index_password]) == $password) {
                    $_SESSION['uid'] = $isFound[$col_index_uid]; // membuat session login
                    return [
                        'status' => true,
                        'message' => 'Anda Berhasil Login'
                    ]; 
                }
            } 
            // Ketika Username atau Password Salah
            return [
                'status' => false,
                'message' => 'Username atau Password Tidak Ditemukan'
            ]; 
        }
        // tidak dapat login ketika belum logout
        return [
            'status' => false,
            'message' => 'Anda Sudah Login'
        ]; 
    }
    public function logout() {
        session_destroy(); // bye dashboard :(
        return true;
    }
}
?>