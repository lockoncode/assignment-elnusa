<?php
namespace App\Models;

use App\Helpers\AppHelper;

class Home {
    public $helper;
    public function __construct() {
        $this->helper = new AppHelper();
        // menerjemahkan data array menjadi kolom (contoh : 1 => name)
        $this->userMap = ['uid','name','email','password'];
    }
    public function getUserData() { // menampilkan data personal (user yg login)
        $helper = $this->helper;

        $uid = $_SESSION['uid']; 
        $col_index_uid = array_search('uid', $this->userMap); // menerjemahkan kolom menjadi index array
        $getData = $helper->searchData($uid, $col_index_uid, 'user'); // mencari data pada dataset
        if ($getData) {
            // menerjemahkan dari array menjadi kolom
            $getData = $helper->mappingArray($getData, $this->userMap); 
            // menampilkan data
            return [
                'data' => $getData,
                'status' => true,
                'message' => 'Ok'
            ];
        } else {
            return [
                'status' => false,
                'message' => 'Data Tidak Ditemukan'
            ];
        }
    }
    public function getAllUserData() { // menampilkan seluruh data dari dataset
        $helper = $this->helper;
        $getData = $helper->showData('user');
        if ($getData) {
            return $getData;
        }
        return [];
    }
}