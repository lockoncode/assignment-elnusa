<?php
namespace App\Helpers;

use Jenssegers\Blade\Blade;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AppHelper {
    public $log;
    function __construct() {
        $log = new Logger('HelperErrors');
        $log->pushHandler(new StreamHandler('log/data.event.log')); // membuat file log
        $this->log = $log;
    }

    function view($file, $data = []) {
        // Menggunakan library Blade Engine untuk menampilkan HTML
        $path = __DIR__ . '../../../views/'; // tempat views berkumpul
        $blade = new Blade($path,  __DIR__ . '../../../cache'); // memanggil blade dan tempat cache
        echo $blade->make($file, $data); // tampilkan hasil render
    } 

    function input(array $array) {
        // mengecek apakah inputan dari FORM sudah terpenuhi atau tidak
        $input = [];
        foreach ($array as $row) {
            if (isset($_POST[$row])) {
                $input[$row] = $_POST[$row];
            } else {
                return false;
            }
        }
        return $input;
    }

    function removeWhiteSpace($string) {
        // menghilangkan permasalahan whitespace ketika menggunakan txt sebagai dataset
        $string = preg_replace('/\s+/', ' ', $string);
        $string = ltrim($string, ' ');
        $string = rtrim($string, ' ');
        return $string;
    }

    function mappingArray($data, $map) {
        // memetakan string menjadi associative array
        $res = [];
        foreach ($map as $m_key => $m_val) {
            if (isset($data[$m_key])) {
                $res[$m_val] = $data[$m_key];
            } // PR:  catch if null
        }
        return $res;
    }
    public function searchData($val_search, $col_index, $data_name) {
        $data = $this->showData($data_name); // menampilkan seluruh data
        if ($data) {
            foreach ($data as $row) {
                // mencari row dengan kriteria tertentu dari seluruh data
                if ($this->removeWhiteSpace($row[$col_index]) == $val_search) {
                    return $row;
                }
            }
        }
        return false;
    }
    public function showData($param) {
        $path = 'db/'.$param.'.txt'; // tempat menampung data
        if (file_exists($path)) {
            $datas = file($path); // mengubah string perline menjadi array
            $data = [];
            foreach ($datas as $key => $val) {
                array_push($data, explode('|', $val)); // membuat kolom dari string
            }
            if (count($data) == 0) {
                // log peringatan ketika data kosong
                $this->log->warning('Data '.$param.' tidak memiliki row'); 
            }
            return $data;
        }
        // Ketika lokasi Dataset tidak ditemukan
        $this->log->error('Path Data '.$param.' tidak ditemukan');
        return false;
    }
    public function writeData($param, $data) {
        $path = 'db/'.$param.'.txt'; // tempat menampung data
        if (file_exists($path)) { 
            $fileData = fopen($path, 'a+'); // membuka file
            $newData = "\r\n".implode('|', $data); // mengubah kolom menjadi string
            fwrite($fileData, $newData); // menyimpan perubahan pada dataset
            fclose($fileData);
            return true;
        }
        // ketika dataset tidak ditemukan
        $this->log->error('Path Data '.$param.' tidak ditemukan');
        return false; 
    }
}
?>