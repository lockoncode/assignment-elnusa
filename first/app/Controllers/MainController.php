<?php
namespace App\Controllers;

use App\Models\Login;
use App\Models\Home;
use App\Models\Register;

class MainController {
    function __construct() {
        $this->login = new Login(); // Fungsi login, logout, dan cek session
        $this->home = new Home(); // Fungsi Home. ketika sudah login
        $this->reg = new Register(); // Fungsi Register akun baru
    }
    function getHome() {
        $login = $this->login; $helper = $login->helper;
        $home = $this->home;

        if ($login->isLogedin()) {

            $dataPersonal = $home->getUserData(); // mengambil data personal akun yg login
            $dataAll = $home->getAllUserData(); // mengambil data akun yang ada di database
            // menampilkan halaman home
            $helper->view('home',['dataPersonal' => $dataPersonal, 'data' => $dataAll]); 
        } else {
            // redirect ke halaman login ketika tidak memiliki akses
            header("Location: ./login/");
        }
    }
    function getLogin() {
        $login = $this->login; $helper = $login->helper;
        if (!$login->isLogedin()) {
            $helper->view('login',['status'=> true]); // menampilkan halaman login
        } else {
            // redirect ke halaman home ketika sudah login
            header("Location: ../");
        }
    }
    function postLogin() {
        $login = $this->login; $helper = $login->helper;
        $inputReq = ['clt_username', 'clt_password'];
        $input = $helper->input($inputReq); // mengecek apakah isian form terisi atau tidak
        if ($input) {
            // mengecek kesesuaian username dan password akun di database 
            $checkAccount = $login->login($input['clt_username'], $input['clt_password']);
            if ($checkAccount['status']) {
                header("Location: ../"); // jika berhasil pergi ke home
            } else {
                $helper->view('login', $checkAccount); // menampilkan pesan kesalahan
            }
        } else {
            // menampilkan error ketika isian form tidak diisi
            $helper->view('login',['status'=> false, 'message'=> 'Username dan Password wajib diisi !']);
        }
    }
    function getLogout() {
        $login = $this->login;
        $login->logout(); // melakukan logout
        header("Location: ./login/"); // redirect ke login
    }

    function getRegistration() {
        $reg = $this->reg; $helper = $reg->helper;
        if (!$this->login->isLogedin()) {
            $helper->view('register', ['status' => true]); // menampilkan halaman register
        } else {
            header("Location: ../"); // redirect to home ketika sudah login
        }
    }

    function postRegistration() {
        $reg = $this->reg; $helper = $reg->helper;
        $inputReq = ['clt_name', 'clt_email', 'clt_username', 'clt_password', 'clt_password_confirm'];
        $input = $helper->input($inputReq); // mengecek kelengkapan isian form
        if ($input) {
            $regSubmit = $reg->submit($input); // mensubmit registrasi
            if ($regSubmit['status']) {
                header("Location: ../"); // redirect ke login jika berhasil submit register
            } else {
                $helper->view('register', $regSubmit); // menampilkan pesan kesalahan
            }
        } else {
            // menampilkan pesan kesalahan
            $helper->view('register',['status'=> false, 'message'=> ['Seluruh Form wajib di isi']]);
        }
    }
}
?>