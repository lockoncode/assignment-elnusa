<?php
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $route) {

    $route->get('/', 'MainController/getHome'); // load home page
    $route->addGroup('/login', function(FastRoute\RouteCollector $r) { // group of login
        $r->get('/', 'MainController/getLogin'); // when load login page
        $r->post('/', 'MainController/postLogin'); // when submit login page
    });
    $route->get('/logout', 'MainController/getLogout'); // logout action
    $route->addGroup('/register', function(FastRoute\RouteCollector $r) { // group of login
        $r->get('/', 'MainController/getRegistration'); // when load register page
        $r->post('/', 'MainController/postRegistration'); // when submit register page
    });
});

// Fetch method and URI from browser
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// dummy URI for first folder; del this if change to production :)
$relative_path =  dirname($_SERVER['SCRIPT_NAME']);
$uri = str_replace($relative_path.'/', '/', $uri);

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo "<h1>404 - Halaman Tidak Ditemukan</h1>";
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo "<h1>405 - Metode Tidak Tersedia</h1>";
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        var_dump($vars, $handler);
        list($class, $method) = explode('/', $handler, 2);
        $controller = 'App\Controllers\\' . $class;
        call_user_func_array(array(new $controller, $method), $vars);
        break;
}
?>