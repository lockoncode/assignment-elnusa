<?php
session_start();
require __DIR__.'/vendor/autoload.php'; // composer auto load

// debug purpose only
// error_reporting(E_ALL);
// error_reporting(1);
// ini_set('display_errors', 1);

// Call config routes to start app (PSR-4) with FastRoute
require __DIR__.'/routes/config-routes.php';
?>