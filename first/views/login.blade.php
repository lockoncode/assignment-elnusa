@extends('layout.main')
@section('content')
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center align-items-center" style="height:100vh">
            <div class="col-md-8 col-lg-5">
                <div class="card">
                    <div class="card-header">Login</div>
                    <div class="card-body">
                        @if($status == false)
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>
                        @endif
                        
                        <form action="./" method="POST">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Username</label>
                                <div class="col-md-6">
                                    <input type="text" id="clt_username" class="form-control" name="clt_username" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="clt_password" class="form-control" name="clt_password" required>
                                </div>
                            </div>
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                                <a href="../register/" class="btn btn-link">
                                    Daftar Akun Baru
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection