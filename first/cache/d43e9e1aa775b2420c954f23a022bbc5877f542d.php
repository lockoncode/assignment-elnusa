<?php $__env->startSection('content'); ?>
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 col-lg-5">
                <div class="card">
                    <div class="card-header">Register</div>
                    <div class="card-body">
                        <?php if($status == false): ?>
                            <div class="alert alert-danger" role="alert">
                                <?php if(count($message)>1): ?>
                                <ul style="margin-bottom:0px;">
                                    <?php $__currentLoopData = $message; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <li> <?php echo e($err); ?> </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <?php endif; ?>
                                <?php if(count($message) == 1): ?>
                                    <?php echo e($message[0]); ?>

                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        
                        <form action="./" method="POST">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" id="clt_name" class="form-control" name="clt_name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Username</label>
                                <div class="col-md-6">
                                    <input type="text" id="clt_username" class="form-control" name="clt_username" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Email</label>
                                <div class="col-md-6">
                                    <input type="email" id="clt_email" class="form-control" name="clt_email" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="clt_password" class="form-control" name="clt_password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Konfirmasi Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="clt_password_confirm" class="form-control" name="clt_password_confirm" required>
                                </div>
                            </div>
                            
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                                <a href="../login/" class="btn btn-link">
                                    Halaman Login
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>