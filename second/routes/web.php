<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Main\HomeController@initHome')->name('home.get');
Route::group(['prefix' => 'login'], function () {
    Route::get('', 'Main\LoginController@getLogin')->name('login.get');
    Route::post('', 'Main\LoginController@submitLogin')->name('login.post');
});
Route::group(['prefix' => 'register'], function () {
    Route::get('', 'Main\RegisterController@getRegister')->name('register.get');
    Route::post('', 'Main\RegisterController@submitRegister')->name('register.post');
});
Route::get('/logout', 'Main\LogoutController')->name('logout');
?>