@extends('layout.master')
@section('content')
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Dashboard</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <a class="btn btn-outline-danger my-2 my-sm-0" href="./logout" role="button">Logout</a>
  </div>
</nav>
<main role="main" class="flex-shrink-0">
  <div class="container">
    <h1 class="mt-5">Selamat Datang</h1>
    <p class="lead">Hai {{$dataPersonal['data']['name']}}, Berikut adalah data user.txt :  </p>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Nama</th>
          <th scope="col">Email</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $key => $row)
          <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$row[0]}}</td>
            <td>{{$row[1]}}</td>
            <td>{{$row[2]}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</main>
@endsection