<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Models\Home;

class HomeController extends Controller{
    function __construct() {
        $this->home = new Home();
    }

    public function initHome() {
        $home = $this->home;
        if ($home->helper->isUserLogin()){
            $dataPersonal = $home->getPersonalInfo(); // mengambil data personal akun yg login
            $dataAll = $home->getUserData(); // mengambil data akun yang ada di database
            return view('home', ['dataPersonal' => $dataPersonal, 'data' => $dataAll]);
        } else {
            return \redirect()->route('login.get');
        }
    }
}
?>