<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Login;

class LoginController extends Controller {
    function __construct() {
        $this->login = new Login();
    }

    function getLogin() {
        $login = $this->login; $helper = $login->helper;
        if(!$helper->isUserLogin()) {
            return view('login', ['status'=>true]);
        }
        return redirect()->route('home.get');
    }

    function submitLogin(Request $request) {
        $login = $this->login; $helper = $login->helper;
        // mengecek apakah isian form terisi atau tidak
        $input = $helper->input(
            $request->input(),
            ['clt_username', 'clt_password']
        ); 
        if ($input) {
            $tryLogin = $login->logginAttempt(
                $input['clt_username'],
                $input['clt_password']
            );
            if ($tryLogin['status']) {
                return redirect()->route('home.get');
            }
            return view('login', $tryLogin);
        }

        return view('
            login', [
                'status'=>false,
                'message'=>'Username dan Password wajib diisi !'
        ]);
        
    }
}
?>