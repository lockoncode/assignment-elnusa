<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Login;

class LogoutController extends Controller {
    // function __contruct() {
    //     $this->login = new Login();
    // }
    function __invoke() {
        // $login = $this->login;
        $login = new Login();
        $logout = $login->logoutAttempt();
        return redirect()->route('login.get');
    }
}

?>