<?php
namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Register;

class RegisterController extends Controller {

    function __construct() {
        $this->register = new Register();
    }

    function getRegister() {
        $reg = $this->register; $helper = $reg->helper;
        if(!$helper->isUserLogin()) {
            return view('register', ['status'=>true]);
        }
        return redirect()->route('home.get');
    }

    function submitRegister(Request $request) {
        $reg = $this->register; $helper = $reg->helper;
        $input = $helper->input(
            $request->input(),
            ['clt_name', 'clt_email', 'clt_username', 'clt_password', 'clt_password_confirm']
        );
        if ($input) {
            $submitRes = $reg->registerAttempt($input);
            if ($submitRes['status']) {
                return redirect()->route('login.get');
            }
            return view('register', $submitRes);
        }
        return view('register', ['status'=> false, 'message'=> ['Seluruh Form wajib di isi']]);
    }
}
?>