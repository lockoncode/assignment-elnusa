<?php
namespace App\Http\Models;

use App\Http\Models\Helper;

class Home {
    public $helper;
    public function __construct() {
        $this->helper = new Helper();
        $this->userMap = $this->helper->userMap;
    }

    public function getPersonalInfo() {
        $helper = $this->helper;

        // mencari data personal berdasarkan session uid
        $uid = session('uid'); 
        $col_index_uid = array_search('uid', $this->userMap); // menerjemahkan kolom menjadi index array
        $getData = $helper->searchData($uid, $col_index_uid, 'user'); // mencari data pada dataset
        if ($getData) {
            // menerjemahkan dari array menjadi kolom
            $getData = $helper->mappingArray($getData, $this->userMap); 
            // menampilkan data
            return [
                'data' => $getData,
                'status' => true,
                'message' => 'Ok'
            ];
        } else {
            return [
                'status' => false,
                'message' => 'Data Tidak Ditemukan'
            ];
        }
    }

    public function getUserData() {
        $helper = $this->helper;
        // menampilkan seluruh data user
        $getData = $helper->showData('user');
        if ($getData) {
            return $getData;
        }
        return [];
    }
}
?>