<?php
namespace App\Http\Models;

use App\Http\Models\Helper;
class Register {
    public $helper;
    public function __construct() {
        $this->helper = new Helper();
        $this->userMap = $this->helper->userMap;
    }

    public function registerAttempt($input) {
        $helper = $this->helper;
        $messages = [];
         // cek ke validan username
         if (strlen($input['clt_username'])<4) { 
            $messages[] = 'Username minimal harus memiliki 4 karakter';
        }

        // cek apakah username sudah digunakan atau tidak
        $col_index_uid = array_search('uid', $this->userMap);
        $checkUsername = $helper->searchData($input['clt_username'], $col_index_uid, 'user');
        if ($checkUsername) {
            $messages[] = 'Username sudah dipakai';
        }

        // cek format penulisan email
        if (!filter_var($input['clt_email'], FILTER_VALIDATE_EMAIL)) {
            $messages[] = 'Format email tidak sesuai';
        }
        
        // cek apakah email sudah digunakan atau tidak
        $col_index_email = array_search('email', $this->userMap);
        $checkEmail = $helper->searchData($input['clt_email'], $col_index_email, 'user');
        if ($checkEmail) {
            $messages[] = 'Email '.$input['clt_email'].' sudah dipakai';
        }

        // cek ke validan password
        if (strlen($input['clt_password'])<6) {
            $messages[] = 'Password minimal harus memiliki 6 karakter';
        }

        // cek kesamaan password dan konfirmasi password
        if ($input['clt_password'] != $input['clt_password_confirm']) {
            $messages[] = 'Password dan Konfirmasi Password tidak sama';
        }

        if (!$messages) {
            // memasukkan data untuk siap di tulis ke dataset
            $data = [$input['clt_username'], $input['clt_name'], $input['clt_email'], $input['clt_password']];
            $writeData = $helper->writeData('user', $data); // menulis data
            if ($writeData) {
                return ['status' => true, 'message' => []]; // jika berhasil 
            }
            // jika gagal menulis log dan melaporkannya
            $helper->log->error('register: Gagal Menulis File'); 
            $messages[] = 'Gagal menambahkan file';
        }
        // ketika ada error maka tampilkan messages
        return ['status' => false, 'message' => $messages];
    }
}
?>