<?php
namespace App\Http\Models;
use Illuminate\Support\Facades\Log;
class Helper {
    // memetakan struktur user.txt
    public $userMap = ['uid','name','email','password'];
    
    public function removeWhiteSpace($string) {
        // menghilangkan whitespace ketika menggunakan txt sebagai dataset
        $string = preg_replace('/\s+/', ' ', $string);
        $string = ltrim($string, ' ');
        $string = rtrim($string, ' ');
        return $string;
        // Log::info('Showing user profile for user: ');
    }

    function input($input, array $require) {
        // mengecek apakah inputan dari FORM sudah terpenuhi atau tidak
        $newInput = [];
        foreach ($require as $req) {
            if (isset($input[$req])) {
                $newInput[$req] = $input[$req]; 
            } else { return false; }
        }

        return $newInput;
        // foreach ($require as $row) {
        //     if (isset($_POST[$row])) {
        //         $input[$row] = $_POST[$row];
        //     } else {
        //         return false;
        //     }
        // }
        return $input;
    }

    function mappingArray($data, $map) {
        // memetakan string menjadi associative array
        $res = [];
        foreach ($map as $m_key => $m_val) {
            if (isset($data[$m_key])) {
                $res[$m_val] = $data[$m_key];
            }
        }
        return $res;
    }
    public function searchData($val_search, $col_index, $data_name) {
        $data = $this->showData($data_name); // menampilkan seluruh data
        if ($data) {
            foreach ($data as $row) {
                // mencari row dengan kriteria tertentu dari seluruh data
                if ($this->removeWhiteSpace($row[$col_index]) == $val_search) {
                    return $row;
                }
            }
        }
        return false;
    }
    public function writeData($param, $data) {
        $path = storage_path('db/'.$param.'.txt'); // tempat menampung data
        if (file_exists($path)) { 
            $fileData = fopen($path, 'a+'); // membuka file
            $newData = "\r\n".implode('|', $data); // mengubah kolom menjadi string
            fwrite($fileData, $newData); // menyimpan perubahan pada dataset
            fclose($fileData);
            return true;
        }
        // ketika dataset tidak ditemukan
        Log::error('Path Data '.$param.' tidak ditemukan');
        return false; 
    }
    public function showData($param) {
        $path = storage_path('db/'.$param.'.txt'); // tempat menampung data
        if (file_exists($path)) {
            $datas = file($path); // mengubah string perline menjadi array
            $data = [];
            foreach ($datas as $key => $val) {
                array_push($data, explode('|', $val)); // membuat kolom dari string
            }
            if (count($data) == 0) {
                // log peringatan ketika data kosong
                Log::warning('Data '.$param.' tidak memiliki row');
            }
            return $data;
        }
        // Ketika lokasi Dataset tidak ditemukan
        Log::error('Path Data '.$param.' tidak ditemukan');
        return false;
    }
    public function isUserLogin() {
        // mengecek apakah memiliki akses login atau tidak
        if (session()->has('uid')) {
            $uid = session('uid');
            $col_index_uid = array_search('uid', $this->userMap); // menerjemahkan kolom ke index array
            $isFound = $this->searchData($uid, $col_index_uid, 'user'); // mengecek kebenaran session
            if ($isFound) { // jika session user sesuai dengan database
                return true;
            }
        } else {
            session()->forget('uid');
            return false;
        }
        /*if(isset($_SESSION['uid'])) {
            $uid = $_SESSION['uid'];
            $col_index_uid = array_search('uid', $this->userMap); // menerjemahkan kolom ke index array
            $isFound = $this->searchData($uid, $col_index_uid, 'user'); // mengecek kebenaran session
            if ($isFound) { // jika session user sesuai dengan database
                return true;
            }
        } else {
            // logout ketika session tidak sesuai dengan dataset 
            if (isset($_SESSION)){
                session_destroy();
            }
            return false;
        }*/
    } 
}

?>