<?php
namespace App\Http\Models;

use App\Http\Models\Helper;

class Login {
    public $helper;
    public function __construct() {
        $this->helper = new Helper();
        $this->userMap = $this->helper->userMap;
    }

    public function logginAttempt($uid, $password) {
        $helper = $this->helper;
        if(!isset($_SESSION['uid'])) {
            $col_index_uid = array_search('uid', $this->userMap);
            $col_index_password = array_search('password', $this->userMap);
            $isFound = $helper->searchData($uid, $col_index_uid, 'user');
            if ($isFound) {
                if (isset($isFound[$col_index_password]) && // mengecek kesesuain password
                    $helper->removeWhiteSpace($isFound[$col_index_password]) == $password) {
                    // $_SESSION['uid'] = $isFound[$col_index_uid]; // membuat session login
                    session(['uid' => $isFound[$col_index_uid]]);
                    return [
                        'status' => true,
                        'message' => 'Anda Berhasil Login'
                    ]; 
                }
            } 
            // Ketika Username atau Password Salah
            return [
                'status' => false,
                'message' => 'Username atau Password Tidak Ditemukan'
            ]; 
        }
        // tidak dapat login ketika belum logout
        return [
            'status' => false,
            'message' => 'Anda Sudah Login'
        ]; 
    }
    public function logoutAttempt() {
        session()->flush(); // bye dashboard :(
        return true;
    }
}
?>